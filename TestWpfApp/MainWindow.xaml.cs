﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace TestWpfApp
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<Students> listStudents = new List<Students>();
        public MainWindow()
        {
            InitializeComponent();
            listBoxStudents.ItemsSource = listStudents;
            LoadData();
        }

        private void LoadData()
        {
            listStudents.Clear();
            var list = XDocument.Load(@"Students.xml").Elements("Students").Elements("Student");

            foreach (var item in list)
            {
                listStudents.Add(new Students
                {
                    Id = int.Parse(item.Attribute("Id").Value),
                    FirstName = item.Element("FirstName").Value,
                    LastName = item.Element("Last").Value,
                    Age = uint.Parse(item.Element("Age").Value),
                    Gender = (Sex)(int.Parse(item.Element("Gender").Value))
                });
            }
            if (listStudents.Count == 0)
            {
                listBoxStudents.Visibility = System.Windows.Visibility.Collapsed;
            }
            else
                listBoxStudents.Visibility = System.Windows.Visibility.Visible;
            ICollectionView view = CollectionViewSource.GetDefaultView(listStudents);
            view.Refresh();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            var resultDialog = MessageBox.Show(
                string.Format("Вы действительно хотите удалить {0}?", listBoxStudents.SelectedItems.Count > 1 ? "записи" : "запись"),
                "Получение разрешения?", MessageBoxButton.OKCancel, MessageBoxImage.Question);
            if (resultDialog == MessageBoxResult.OK)
            {
                foreach (Students item in listBoxStudents.SelectedItems)
                {
                    XElement studs = XDocument.Load(@"Students.xml").Element("Students");
                    studs.Descendants("Student").Where(x => x.Attribute("Id").Value == item.Id.ToString()).Remove();

                    studs.Save(@"Students.xml");
                }

                LoadData();
            }
        }
        private void EditStudent()
        {

        }


        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            EditStudentWindow window = new EditStudentWindow(null);
            window.ShowDialog();
            LoadData();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            EditStudentWindow window = new EditStudentWindow(listStudents[listBoxStudents.SelectedIndex]);
            window.ShowDialog();
            LoadData();
        }
    }
}
