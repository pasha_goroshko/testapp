﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml.Linq;

namespace TestWpfApp
{
    /// <summary>
    /// Interaction logic for EditStudentWindow.xaml
    /// </summary>
    public partial class EditStudentWindow : Window
    {
        Students student;
        public EditStudentWindow(Students _student)
        {
            InitializeComponent();
            this.student = _student;
            if (_student != null)
            {
                FirsNameBox.Text = student.FirstName;
                LastNameTextBox.Text = student.LastName;
                AgeBox.Text = student.Age.ToString();
                GenderComboBox.SelectedIndex = (int)student.Gender;
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void AddStudent(string firstName, string lastName, int? age, Sex gender)
        {
            XElement studs = XDocument.Load(@"Students.xml").Element("Students");
            if (student == null)
            {
                var count = studs.Descendants("Student").Count();
                studs.Add(
                    new XElement(new XElement("Student", new XAttribute("Id", count),
            new XElement("FirstName", firstName),
            new XElement("Last", lastName),
            new XElement("Age", age == null ? "" : age.ToString()),
            new XElement("Gender", Convert.ToInt32(gender)))));

            }
            else
            {
                foreach (var item in studs.Elements("Student"))
                {
                    if (item.Attribute("Id").Value == student.Id.ToString())
                    {
                        item.Element("FirstName").Value = firstName;
                        item.Element("Last").Value = lastName;
                        item.Element("Age").Value = (age).ToString();
                        item.Element("Gender").Value = ((int)gender).ToString();
                    }
                }
            }
            studs.Save(@"Students.xml");
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            try
            {
                //Обрабатываем ошибку на возраст с помощью THROW
                if (int.Parse(AgeBox.Text) >= 16 && int.Parse(AgeBox.Text) <= 100)
                {
                    //Обрабатываем на пустые строки с помощью метода IsNullOrWhiteSpace
                    if (!string.IsNullOrWhiteSpace(FirsNameBox.Text) && !string.IsNullOrWhiteSpace(LastNameTextBox.Text) && !string.IsNullOrWhiteSpace(GenderComboBox.Text))
                    {
                        AddStudent(FirsNameBox.Text, LastNameTextBox.Text, int.Parse(AgeBox.Text), (Sex)GenderComboBox.SelectedIndex);
                        MessageBox.Show("Запись успешно обработана!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Information);
                        Close();
                    }
                    else
                        MessageBox.Show("Поля с именем, фамилией и полом обязательны для заполнения!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
                else
                    throw new InvalidOperationException("Возраст не может быть отрицательным и должен находиться в диапазоне [16, 100]!");
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show(ex.Message);
            }
            catch(Exception ex) 
            { MessageBox.Show(ex.Message); }
        }

        private void AgeBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            if (!Char.IsDigit(e.Text, 0)) e.Handled = true;
        }
    }
}
