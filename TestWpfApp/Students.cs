﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestWpfApp
{
    public enum Sex
    {
        Male,
        Female
    }
    public class Students
    {
        public string FullName
        {
            get { return string.Format("{0} {1}",LastName,FirstName); }
        }
        public string FullAgeToString
        {
            get
            {
                switch ((Age % 10))
                {
                    case 1: return string.Format("{0} год", Age);
                    case 2:
                    case 3:
                    case 4: return string.Format("{0} года", Age);
                    default: return string.Format("{0} лет", Age);
                }
            }
        }
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public uint Age { get; set; }
        public Sex Gender { get; set; }
    }
}
